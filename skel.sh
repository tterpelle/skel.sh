#!/bin/sh

SCRIPT="skel.sh"
AUTHOR="Tristan Terpelle (tristan@terpelle.be)"
DATE="01/Jan/1900"
VERSION="0.00"
URL="https://bitbucket.org/tterpelle/skel.sh"

# DESCRIPTION OF SCRIPT

# ----- START CONFIGURATION ---------------------
# default variables
# error status
_ERR="0"
# file where the script's output will go to
_OUTPUTFILE="/tmp/$(basename $0).out.$$"

# ----- END CONFIGURATION -----------------------

# ----- SCRIPT FUNCTIONS ------------------------

# logging: logs messages to $_OUTPUTFILE
log() {
	_LOG="$@"
	echo $_LOG >> $_OUTPUTFILE
}

# send mail with script results
sendMail() {
	mailx -s "$_SUBJECT" $_MAILTO < $_OUTPUTFILE 
}

# clean up temprory files
cleanUp() {
	rm $_OUTPUTFILE
}

# send mail with error report, clean up and exit with a failure
error() { 
	_REASON="$@"
	_SUBJECT="$_SUBJECT - ERROR: $_REASON"
	if [ -n "$_MAILTO" ]
	then 
		sendMail
		cleanUp
	else
		echo "error: $_REASON"
	fi
	exit 1
}

# finish script 
finish() {
	_SUBJECT="$_SUBJECT - SUCCESS"
	if [ -n "$_MAILTO" ]
	then 
		sendMail
		cleanUp
	else
		echo "done: see $_OUTPUTFILE"
	fi
	exit 0
}

# usage
usage() {
cat << EOF
Usage: $0 -a param1 -b param2 -c param3 [-h|-v]

This script provides a base for a shell script.
       
	 -a param1        parameter 1
	 -b param2        parameter 2
	 -c param3        parameter 3
	 -h               help (all this)
	 -v               version info
EOF
	exit 5
}

# version
version() {
cat << EOF
$SCRIPT v$VERSION ($DATE), by $AUTHOR
EOF
	exit 10
}

# ----- END SCRIPT FUNCTIONS --------------------

# ----- SCRIPT START ----------------------------
# parse script options and parameters
# more info on how to use getopts: http://wiki.bash-hackers.org/howto/getopts_tutorial
while getopts ":a:b:c:vh" opt
do
	case $opt in
		a) _A="$OPTARG"
		;;
		
		b) _B="yes"
		;;
		
		c) _C="$OPTARG"
		;;
		
		h) usage
		;;
		
		v) version
		;;
		
		*) usage
		;; 
	esac
done

touch $_OUTPUTFILE || error "could not create output file $_OUTPUTFILE"

# log some info that will be useful for debugging purposes
log "Script started at $(date) on $(hostname)"
log "====="
log "\$0 = $0"
log "args($#)=\"$@\""
log "_A=$_A"
log "_B=$_B"
log "_C=$_C"
log "-----"

# check if script isn't running already and error out if it is
if [ $(pgrep -l $0) ]
then
	error "script $0 already running"
fi 

# verify arguments passed to script
if [ -z "$_A" -o -z "$_B" -o -z "$_C" ]
then
	error "arguments passed to $0 invalid: _A = \"$_A\", _B = \"$_B\", _C = \"$_C\""
fi

# insert actual code here

log "script finished at $(date)"
log "====="

finish

# it should never come this far, so exit with non-zero code
exit 255
# ----- SCRIPT END ------------------------------
